#!/usr/bin/env python

from client import Client

class Events(Client):

    def all(self, page=1, since=False):
        action = 'events.json'

        if page > 1:
            action = '%s?page=%s' % (action, page)

        if since != False:
            if page > 1:
                action = '%s&since=%s' (action, since)
            else:
                action = '%s?since=%s' (action, since)
