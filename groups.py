#!/usr/bin/env python

from client import Client
from group import Group
from subgroup import SubGroup

class Groups(Client):

    @property
    def all(self):
        return self.client.get('groups.json')

    def get(self, group_id):
        group = Group(group_id)
        return group

    def create(self, name, memeberships):
        payload = {
            "name": name,
            "memeberships": []
        }

        for m in memeberships:
            payload["memeberships"].append({"person_id":m})

        return self.client.post("groups.json", payload)

    def delete(self, group_id):
        return self.client.delete("groups/%s.json" % (group_id))


class SubGroups(Client):

    def __init__(self, group_id):
        super(SubGroups, self).__init__()
        self.group_id = group_id

    @property
    def get(self, subgroup_id):
        subgroup = SubGroup(self.group_id, subgroup_id)
        return subgroup

    def create(self, name, memeberships):
        payload = {
            "name": name,
            "memeberships": []
        }

        for m in memeberships:
            payload["memeberships"].append({"person_id":m})

        return self.client.post("groups/%s/subgroups.json" % (self.group_id), payload)

    def delete(self, subgroup_id):
        return self.client.delete("groups/%s/subgroups/%s.json" % (self.group_id, subgroup_id))
