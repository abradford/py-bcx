#!/usr/bin/env python

import requests, json
from httpcache import CachingHTTPAdapter
from settings import (
        USER,
        PASS,
        HEADERS,
        BASE_URI
    )

class BaseClient(object):

    def __init__(self, test=False):
        self.session = requests.Session()
        self.session.mount('http://', CachingHTTPAdapter())
        self.session.mount('https://', CachingHTTPAdapter())
        self.session.auth = (USER,PASS)
        self.session.headers.update(HEADERS)
        self.base_uri = BASE_URI
        self.test = test

    def get(self, action):
        url = self.base_uri+action
        r = self.session.get(url)
        return self.checkResponse(r, 200)

    def put(self, action, payload):
        url = self.base_uri+action
        r = self.session.put(url, data=json.dumps(payload))
        return self.checkResponse(r, 200)

    def post(self, action, payload):
        url = self.base_uri+action
        r = self.session.post(url, data=json.dumps(payload))
        return self.checkResponse(r, 201)

    def delete(self, action):
        url = self.base_uri+action
        r = self.session.delete(url)

        if r.status_code == 204:
            return True
        else:
            return r.status_code

    def checkResponse(self, request, success_code):
        if self.test == True:
            return request
        else:
            if request.status_code != success_code:
                return request.status_code
            else:
                return request.json()

class Client(object):

    def __init__(self):
        self.client = BaseClient()

class TestClient(object):

    def __init__(self):
        self.client = BaseClient(True)

