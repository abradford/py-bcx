#!/usr/bin/env python

from client import Client

class Stars(Client):

    @property
    def all(self):
        return self.client.get('stars.json')

    def star(self, project_id):
        payload = {}
        return self.client.post('projects/%s/star.json' % (project_id), payload)

    def unstar(self, project_id):
        return self.client.delete('projects/%s/star.json' % (project_id))
