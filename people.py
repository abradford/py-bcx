#!/usr/bin/env python

from client import Client
from person import Person

class People(Client):

    @property
    def all(self):
        return self.client.get('people.json')

    @property
    def trashed(self):
        return self.client.get('people/trashed.json')

    @property
    def me(self):
        return self.client.get('people/me.json')

    def get(self, person_id):
        person = Person(person_id)
        return person

    def delete(self, person_id):
        return self.client.delete('people/%s.json' % (person_id))



