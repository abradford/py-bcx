#!/usr/bin/env python

from client import Client
from project import Project

class Projects(Client):

    @property
    def all(self):
        # all minus the templates
        return self.client.get('projects.json')

    @property
    def drafts(self):
        return self.client.get('projects/drafts.json')

    @property
    def archived(self):
        return self.client.get('projects/archived.json')

    @property
    def templates(self):
        return self.client.get('project_templates.json')

    @property
    def topics(self):
        return self.client.get('topics.json')

    @property
    def archived_topics(self):
        return self.client.get('topics/archived.json')


    def get(self, project_id):
        project = Project(project_id)
        return project

    # Need to update this so that it can create
    # projects based off templates
    def create(self, name, description):
        payload = {
            'name': name,
            'description': description
        }
        return self.client.post('projects.json', payload)

    def archive(self, project_id):
        payload = {"archived": True}
        return self.client.put('projects/%s.json' % (project_id), payload)

    def activate(self, project_id):
        payload = {"archived": False}
        return self.client.put('projects/%s.json' % (project_id), payload)

    def delete(self, project_id):
        pass

