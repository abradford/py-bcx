#!/usr/bin/env python
#
# This is a sample settings.py file
# Right now I'm only setup to use basic auth
#
# To get started:
# Rename this file to settings.py
# Replace email, password, basecamp_id &&& user_agent
#

USER = 'email'
PASS = 'password'
BASE_URI = 'https://basecamp.com/basecamp_id/api/v1/'
HEADERS = {
    'content-type': 'application/json',
    'user-agent': 'user_agent'
}
