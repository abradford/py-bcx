#!/usr/bin/env python

from client import Client

class Person(Client):

    def __init__(self, person_id):
        super(Person, self).__init__()
        self.id = person_id

    @property
    def details(self):
        return self.client.get('people/%s.json' % (self.id))

    @property
    def projects(self):
        return self.client.get('people/%s/projects.json' % (self.id))

    def events(self, page=1, since=False):
        action = 'people/%s/events.json' % (self.id)

        if page > 1:
            action = '%s?page=%s' % (action, page)

        if since != False:
            if page > 1:
                action = '%s&since=%s' (action, since)
            else:
                action = '%s?since=%s' (action, since)

        return self.client.get(action)

    def assigned_todos(self, since=False):
        action = 'people/%s/assigned_todos.json' % (self.id)
        if since != False:
            action = '%s?due_since=%s' % (action, since)

        return self.client.get(action)
