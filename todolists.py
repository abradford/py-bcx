#!/usr/bin/env python

from client import Client
from todolist import Todolist

class Todolists(Client):

    @property
    def all(self):
        return self.client.get('todolists.json')

    @property
    def completed(self):
        return self.client.get('todolists/completed.json')

    @property
    def trashed(self):
        return self.client.get('todolists/trashed.json')

    def get(self, project_id, todolist_id):
        todolist = Todolist(project_id, todolist_id)
        return todolist

    def create(self, project_id, name, description):
        payload = {
            "name": name,
            "description": description
        }
        return self.client.post('projects/%s/todolists.json' % (project_id), payload)

    def delete(self, project_id, todolist_id):
        return self.client.delete('projects/%s/todolists/%s.json' (project_id, todolist_id))


