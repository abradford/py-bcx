# Sample Usage #

```
#!python

from py-bcx.projects import Projects

p = Projects()
print p.all
print p.drafts
print p.archived
print p.templates

```

# all #

Returns all of the basecamp projects minus the templates

```
#!python

from py-bcx.projects import Projects

p = Projects()
print p.all

```

# drafts #
Returns all of the basecamp projects that are drafts.
Drafts are created when you make a project from a project template
You must publish a draft for it to be viewable to everyone else that has access to the project

```
#!python

from py-bcx.projects import Projects

p = Projects()
print p.drafts

```

# archived #
Returns all of the archived projects

```
#!python

from py-bcx.projects import Projects

p = Projects()
print p.archived

```

# templates #
Returns all of the project templates

```
#!python

from py-bcx.projects import Projects

p = Projects()
print p.templates

```

# topics #
Returns all of the topics inside basecamp.
This property needs to be updated to a function in order to include pagination.  Currently only returns the first 50 topics
```
#!python

from py-bcx.projects import Projects

p = Projects()
print p.topics

```

# archived_topics #
Returns all of the archived topics inside basecamp.
This property needs to be updated to a function in order to include pagination.  Currently only returns the first 50 archived topics
```
#!python

from py-bcx.projects import Projects

p = Projects()
print p.archived_topics

```

# get(project_id) #
Returns Project object
```
#!python

from py-bcx.projects import Projects

p = Projects()
project = p.get(123)

print project.details

```
