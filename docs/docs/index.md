# Getting Started #

Python library for the new basecamp api
[https://github.com/basecamp/bcx-api/](https://github.com/basecamp/bcx-api/)


Uses the following

* [Requests](http://docs.python-requests.org/en/latest/)
* [httpcache](https://httpcache.readthedocs.org/en/latest/)
* [json](https://docs.python.org/2/library/json.html)

## Settings ##

To use rename the settings-sample.py to settings.py and update the settings.py file

```
#!python

# To get started:
# Rename this file to settings.py
# Replace email, password, basecamp_id &&& user_agent

USER = 'email'
PASS = 'password'
BASE_URI = 'https://basecamp.com/basecamp_id/api/v1/'
HEADERS = {
    'content-type': 'application/json',
    'user-agent': 'user_agent'
}

```
# BaseClient  #

BaseClient initiates the http session, mounts the caching adapter and does all of the requests to basecamp itself.

BaseClient has 5 functions:
* get
* put
* post
* delete
* checkResponse

## Sample Usage ##

```
#!python

bc = BaseClient()
print bc.get('projects.json')

bc = BaseClient(True)
print bc.get('projects.json').status_code
print bc.get('projects,json').json()

```

## BaseClient.get(action) ##
Gets data from the given action
Returns either an error code, json object, or request object

```
#!python

bc = BaseClient()
print bc.get('projects.json')

bc = BaseClient(True)
print bc.get('projects.json').status_code
print bc.get('projects,json').json()

```

## BaseClient.put(action, payload) ##
Puts data to the given action
Returns either an error code, json object, or request object

```
#!python

bc = BaseClient()
project_id = 123
payload = {
    "archived": True
}

print bc.put('projects/%s.json' % (project_id), payload)

```

# Client #

Client is the base object used across py-bcx for calls to basecamp.
It sets self.client = BaseClient() all calls through Client return either json or error code

```
#!python

c = Client()
print c.client.get('projects.json')

```

# TestClient #

I use this for testing calls before coding them.
It sets self.client = BaseClient(test=True). This returns the full request for debugging.

```
#!python

tc = TestClient()
print tc.client.get('projects.json')
print tc.client.get('projects.json').status_code
print tc.client.get('projects,json').json()

```

#  Ready for Use  #

* [Projects](https://bitbucket.org/abradford/py-bcx/wiki/Projects)
* Project Templates
* Stars
* People
* Accesses
* Companies/Groups
* Events
