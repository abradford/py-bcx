#!/usr/bin/env python

from client import Client

class Project(Client):

    def __init__(self, project_id):
        super(Project, self).__init__()
        self.id = project_id

    @property
    def details(self):
        return self.client.get('projects/%s.json' % (self.id))

    @property
    def access(self):
        return self.client.get('projects/%s/accesses.json' % (self.id))

    @property
    def archived_topics(self):
        return self.client.get('projects/%s/topics/archived.json' % (self.id))

    @property
    def todolists(self):
        return self.client.get('projects/%s/todolists.json' % (self.id))

    @property
    def completed_todolists(self):
        return self.client.get('projects/%s/todolists/completed.json' % (self.id))

    @property
    def trashed_todolists(self):
        return self.client.get('projects/%s/todolists/trashed.json' % (self.id))

    def topics(self, page=1, sort='newest'):
        action = 'projects/%s/topics.json' % (self.id)

        if page > 1:
            action = '%s?page=%s' % (action, page)

        if sort != 'newest':
            if page > 1:
                action = '%s&sort=%s' % (action, sort)
            else:
                action = '%s?sort=%s' % (action, sort)
        return self.client.get(action)

    def events(self, page=1, since=False):
        action = 'projects/%s/events.json' % (self.id)

        if page > 1:
            action = '%s?page=%s' % (action, page)

        if since != False:
            if page > 1:
                action = '%s&since=%s' (action, since)
            else:
                action = '%s?since=%s' (action, since)

        return self.client.get(action)

    def grant_access(self, client_access=False, **kwargs):
        if kwargs is not None:
            access_type = "accesses"
            if client_access:
                access_type = "client_access"

            payload = {}
            for key, value in kwargs.iteritems():
                payload[key] = value

            print payload
            print access_type

            return self.client.post('projects/%s/%s.json' % (self.id, access_type), payload)
        else:
            return "Supply either ids or email_addresses"

    def grant_client_access(self, **kwargs):
        return self.grant_access(True, **kwargs)

    def revoke_access(self, person_id):
        return self.client.delete('projects/%s/accesses/%s.json' % (self.id, person_id))



