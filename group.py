#!/usr/bin/env python

from client import Client

class Group(Client):

    def __init__(self, group_id):
        super(Group, self).__init__()
        self.id = group_id

    @property
    def details(self):
        return self.client.get('groups/%s.json' % (self.id))

    def update(self, name):
        payload = {"name": name}

        self.client.put("groups/%s.json" % (self.id), payload)

    def add_members(self, memberships):
        payload = {"memberships":[]}

        for m in memberships:
            payload["memberships"].append({"person_id":m})

        return self.client.put("groups/%s.json" % (self.id), payload)

    def delete_memeber(self, person_id):
        return self.client.delete("groups/%s/memberships/%s.json" % (self.id, person_id))
