#!/usr/bin/env python

from client import Client

class Todolist(Client):

    def __init__(self, project_id, todolist_id):
        super(Todolist, self).__init__()
        self.project_id = project_id
        self.id = todolist_id

    @property
    def details(self):
        return self.client.get('projects/%s/todolists/%s.json?exclude_todo=true' % (self.project_id, self.id))

    @property
    def todos(self):
        todolist = self.client.get('projects/%s/todolists/%s.json' % (self.project_id, self.id))
        if isinstance(todolist, dict):
            return todolist["todos"]
        else:
            return todolist

    def update(self, name, description):
        payload = {
            "name": name,
            "description": description
        }
        return self.client.put('projects/%s/todolists/%s.json' % (self.project_id, self.id), payload)

    def reorder(self, position):
        payload = {
            "position": position
        }
        return self.client.put('projects/%s/todolists/%s.json' % (self.project_id, self.id), payload)

    def make_private(self):
        payload = {
            "private": True
        }
        return self.client.put('projects/%s/todolists/%s.json' % (self.project_id, self.id), payload)

    def make_public(self):
        payload = {
            "private": False
        }
        return self.client.put('projects/%s/todolists/%s.json' % (self.project_id, self.id), payload)


