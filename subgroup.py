#!/usr/bin/env python

from client import Client


class SubGroup(Client):

    def __init__(self, group_id, subgroup_id):
        super(SubGroup, self).__init__()
        self.group_id = group_id
        self.id = subgroup_id

    @property
    def details(self):
        return self.client.get(
            'groups/%s/subgroups/%s.json' % (self.group_id,self.id))

    def update(self, name):
        payload = {"name": name}

        self.client.put(
            "groups/%s/subgroups/%s.json" % (self.group_id, self.id), payload)

    def add_members(self, memberships):
        payload = {"memberships":[]}

        for m in memberships:
            payload["memberships"].append({"person_id":m})

        return self.client.put(
            "groups/%s/subgroups/%s.json" % (self.group_id, self.id), payload)

    def delete_memeber(self, person_id):
        return self.client.delete(
            "groups/%s/subgroups/%s/memberships/%s.json" % (self.group_id, self.id, person_id))
